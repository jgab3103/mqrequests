from django.conf.urls import patterns, include, url  

urlpatterns = patterns('', 

	url(r'^all_requests', 'staffRequests.views.all_requests'),
	url(r'^submit_request', 'staffRequests.views.submit_request'),
	url(r'^create_tt/$', 'staffRequests.views.create_tt'),
	url(r'^create_unit_change/$' , 'staffRequests.views.create_unit_change'),
	url(r'^request_tt/(?P<request_id>\d+)/$', 'staffRequests.views.request_tt'),
	url(r'^request_pdf/$', 'staffRequests.views.request_pdf'),
	url(r'^delete_note/(?P<note_id>\d+)/$', 'staffRequests.views.delete_note'),
	url(r'^search/$', 'staffRequests.views.search_titles'),


)	
