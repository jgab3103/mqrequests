from django.db import models
from django.contrib.auth.models import User 

class Delegate(models.Model):
	APPROVAL_LEVEL_CHOICES = (
		('', ''),
		(1, 'Request arrived in Science Student Centre'),
		(2, 'Request sent to Department'),
		(3, 'Request decided and sent back to Faculty for Action'),
		(4, 'Request finalised'),
		)
	user = models.ForeignKey(User)
	leveldelegatecanapprove = models.IntegerField(default=0, choices=APPROVAL_LEVEL_CHOICES)

	def __unicode__(self):
		return str(self.user.username)

class RequestTypeForDelegate(models.Model):
	REQUEST_TYPE_CHOICES = (
		('TT', 'Timetable'),
		('ICC', 'Individual Case Request'),
		('DC', 'Disciplinary Committee'),
		)
	delegate = models.ForeignKey(Delegate)
	requesttype = models.CharField(max_length=10, choices=REQUEST_TYPE_CHOICES)

	def __unicode__(self):
		return self.requesttype

class EducationalPackage(models.Model):
	EDUC_TYPE_CHOICES = (
		('UN', 'Unit'),
		('CO', 'Course'),
		('ST', 'Student'),
	)
	requesttype_for_delegate = models.ForeignKey(Delegate)
	eductype = models.CharField(max_length=2, choices= EDUC_TYPE_CHOICES)
	educname = models.CharField(max_length=10)
	educcode = models.CharField(max_length=20)

	def __unicode__(self):
		return str(self.eductype) + str(self.educname) + str(self.educcode)



