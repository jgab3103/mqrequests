from django.shortcuts import render_to_response 
from django.http import HttpResponseRedirect
from .models import Notification


def show_notification(request, notification_id):
	n = Notification.objects.get(id = notification_id)
	args = {}
	args['notification'] = n
	return render_to_response('notification.html', args)

def delete_notification(request, notification_id):
	n = Notification.objects.get(id=notification_id)
	n.viewed = True
	n.save()
	return HttpResponseRedirect('/accounts/loggedin')