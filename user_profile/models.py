from django.db import models
from django.contrib.auth.models import User 


UNIT_CHOICES = (
	('MATH101', 'MATH101 - Great Matematicians'),
	('ECON101', 'ECON101 - Micro Economics'),
	('MUS204', 'MUS204 - Sound Explorations'),
	('STAT120', 'STAT120 - Multivariate Methods'),
	)



class UnitApprover(models.Model):
	user = models.ForeignKey(User)
	unitForApproval = models.CharField(max_length=8,
										choices= UNIT_CHOICES,
										default="MUS204")
	def __unicode__(self):
		return str(self.user)

class ICCApprover(models.Model):
	user = models.ForeignKey(User)
	unitForApproval = models.CharField(max_length=20)


class TTApprover(models.Model):
	user = models.ForeignKey(User)
	ApprovalLevel= models.CharField(max_length=20)

class DCApprover(models.Model):
	user = models.ForeignKey(User)
	ApprovalLevel = models.CharField(max_length=20)