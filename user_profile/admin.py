from django.contrib import admin 
from user_profile.models import UnitApprover, ICCApprover, TTApprover, DCApprover



admin.site.register(UnitApprover)
admin.site.register(ICCApprover)
admin.site.register(TTApprover)
admin.site.register(DCApprover)